import React from 'react'
import {withRouter} from 'react-router-dom'

function InnerComponent (props) {
    console.log('inner props', props)
    return <div>
        Inner component
    </div>
}

export default withRouter(InnerComponent)
