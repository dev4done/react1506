import React, {Component} from 'react'
import PropTypes from 'prop-types';
import Inner from './InnerComponent'

class Main extends Component {
    render(){
        console.log(this.props)
        return <div>
           here is product {this.props.match.params.id}
           <Inner/>
        </div>
    }
}

Main.propTypes = {
    match: PropTypes.string.isRequired
}

export default Main
