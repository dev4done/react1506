import React, { Component } from 'react'

import Layout from './Layout'

export default function RenderProps () {
    return <Layout
        sidebar={(data) => {
            return <ul>
                {data.map((item) =>
                    <li key={item}>{item}</li>)
                }
            </ul>
        }
        }
        header={<div>Header</div>}
    />

}
