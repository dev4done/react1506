import React, { Component } from 'react'

const list = ['a', 'b', 'c']
export  default class Layout extends Component{

    render () {

        return <div>
            {this.props.header}
            {this.props.sidebar(list)}
        </div>
    }
}
