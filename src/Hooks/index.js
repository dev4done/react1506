import React, { useState, useEffect } from 'react';

export default function Hooks() {
    // Declare a new state variable, which we'll call "count"
    const [count, setCount] = useState(0);
    const [number, setNumber] = useState(0);

    useEffect(() => {
        // Update the document title using the browser API

        document.title = `You clicked ${count} times`;

        return function () {
            console.log('unmount')
        }
    });

    useEffect(() => {
        console.log(count, number)
    });

    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>
                Click me
            </button>

            <p>You clicked {number} times</p>
            <button onClick={() => setNumber(number + 1)}>
                Click me
            </button>
        </div>
    );
}
