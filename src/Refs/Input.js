import React, { Component } from 'react'

// export default class Input extends Component {
//     constructor (props) {
//         super(props)
//         this.myRef = React.createRef()
//     }
//
//     focusInput = (e) => {
//         this.myRef.current.focus()
//         console.log('focusInput')
//     }
//
//     render () {
//         return <input ref={this.myRef} type="text"/>
//     }
// }

const Input = React.forwardRef((props, ref) => (
    <input ref={ref} type="text"/>
));

export default Input
