import React, { Component, Fragment } from 'react'

import Input from './Input'

export default class Refs extends Component {
    constructor (props) {
        super(props)
        this.input = React.createRef()
    }

    focus = (e) => {
        e.preventDefault()
        this.input.current.focus()
    }

    render () {
        return <>
            <form>
                <Input ref={this.input} type="text"/>
                {/*<Input ref={(ref)=> this.input = ref} type="text"/>*/}
                <button onClick={this.focus}>Click</button>
            </form>
            <p>Here is text</p>
        </>
    }
}
