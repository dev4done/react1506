import React, { Component } from 'react'

class Header extends Component {
  constructor (props){
    super(props)

    this.state = {
      showCount: false,
      test: '',
      counter: 0
    }

    console.log('constructor')
  }
  componentDidUpdate (prevProps) {
    if(prevProps.counter !== this.props.counter){
      this.setState({counter: this.props.counter})
    }
  }

  onClick =  () => {
    this.setState((state) => ({ showCount: !state.showCount }),
      ()=>{
    })
  }

  componentDidMount () {
    console.log('componentDidMount')
  }

  addToCounter = ()=>{
    this.setState((state)=> ({counter: +state.counter + 1}))
  }

  render () {
    const { showCount, counter } = this.state
    console.log('render')
    return <div>
      <button
        onClick={this.onClick}>{showCount ? 'Hide' : 'Show'} count
      </button>
      <button
        onClick={this.addToCounter}>Add to Counter
      </button>
      <p>counter: {counter}</p>
      {showCount && <p>Users: {this.props.users.length}</p>}
    </div>
  }
}

export default Header
