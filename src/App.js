import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link, NavLink, Redirect, Switch } from 'react-router-dom'
import './App.css'

import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'

const theme = createMuiTheme({
    palette: {
        primary: purple,
        secondary: green,
    },
    status: {
        danger: 'orange',
    },
})

import purple from '@material-ui/core/colors/purple'
import green from '@material-ui/core/colors/green'

import Button from '@material-ui/core/Button';

import Refs from './Refs'
import HOC from './HOC'
import RenderProps from './RenderProps'
import ConditionRender from './ConditionRender'
import Portal from './Portal'
import Dialog from './Dialog'
import Context from './Context'
import Hooks from './Hooks'
import Main from './Main'
import PrivateRoute from './PrivateRoute'
import {routes} from './router'

class App extends Component {
    render () {
        return (
            <Router>
                <div className="test">
                    {/*<Refs/>*/}
                    {/*<HOC/>*/}
                    {/*<RenderProps/>*/}
                    {/*<ConditionRender/>*/}
                    {/*<Portal/>*/}
                    {/*<Dialog/>*/}
                    {/*<Context/>*/}
                    {/*<div>*/}
                    {/*    <NavLink*/}
                    {/*        to={'/product/test-product'}*/}
                    {/*    >Go to product</NavLink><br/>*/}
                    {/*    <NavLink exact to={'/'}>Root page</NavLink>*/}
                    {/*    <a href={'/product/test-product'}>Go to product</a>*/}
                    {/*</div>*/}
                    {/*<Route path="/product/:id" component={Main}/>*/}
                    {/*<Route path="/main/:id" component={Main}/>*/}
                    {/*<Route exact path="/hooks" component={Hooks}/>*/}
                    {/*<Route exact path="/redirect" render={()=> <Redirect to={"/main/redirect"}/>}/>*/}

                    {/*<Switch>*/}
                    {/*    <Route path="/product/:id" component={Main}/>*/}
                    {/*    <Route path="/hooks" component={Hooks}/>*/}
                    {/*    <Route render={()=> <div>404</div>}/>*/}
                    {/*</Switch>*/}

                    <Switch>
                        {routes.map((props)=><Route {...{...props}} />)}
                    </Switch>

                </div>

                <ThemeProvider theme={theme}>
                    <Button>Click</Button>
                </ThemeProvider>
            </Router>
        )
    }
}

export default App
