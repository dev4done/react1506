import React from 'react'

const components = {
    input: <input/>,
    button: <button/>,
}

export default function Item ({type}) {
    const Elem = components[type]

    return <Elem/>
}
