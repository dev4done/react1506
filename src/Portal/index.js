import React, { Component } from 'react'
import * as ReactDOM from 'react-dom'


import Refs from '../Refs'

export default function PortalExample () {
    return ReactDOM.createPortal(
        <Refs/>,
        document.getElementById('modal'),
    );
}
