import React, { Component } from 'react'
import List from './List'

function withPosts (EnheritComponent) {
    return class WithPosts extends Component {
        state = {
            posts: []
        }

        async componentDidMount () {
            const posts = await fetch('https://jsonplaceholder.typicode.com/posts')
                .then(res => res.json())

            this.setState({ posts })
        }

        render () {
            return <EnheritComponent list={this.state.posts}/>
        }
    }
}

export default withPosts(List)

