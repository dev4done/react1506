import Main from './Main'
import Hooks from './Hooks'

export const urls = {
    root: '/',
    hooks: '/hooks'
}

export const routes = [
    {
        path: urls.root,
        exact: true,
        component: Main
    },
    {
        path: urls.hooks,
        exact: true,
        component: Hooks
    },
]
