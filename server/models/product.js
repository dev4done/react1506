const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Category'
    },
    description: {
        type: String
    },
    cost: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Product', productSchema);
