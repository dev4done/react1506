const Product = require('../models/product')

const getAll = () => Product.find().populate(['category'])
const create = (product) => Product.create(product)
const update = ({ _id, ...rest }) => Product.update({ _id }, rest)

module.exports = {
    update,
    getAll,
    create
}
