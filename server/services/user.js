const User = require('../models/user')

const getAll = () => User.find()
const create = (product) => User.create(product)
const update = ({ _id, ...rest }) => User.update({ _id }, rest)

module.exports = {
    update,
    getAll,
    create
}
