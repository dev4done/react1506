const productService = require('../services/products')
async function getAll (req, res) {
    const data = await productService.getAll()
    res.json(data)
}

async function create (req, res, next) {
    console.log('req.body', req.body)
    try{
        const data = await productService.create(req.body)
        res.json(data)
    } catch (e) {
        next(e)
    }
}
async function update (req, res, next) {
    try{
        const data = await productService.update(req.body)
        res.json(data)
    } catch (e) {
        next(e)
    }
}

module.exports = {
    getAll,
    create,
    update
}
