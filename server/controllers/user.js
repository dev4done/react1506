const userService = require('../services/user')
async function getAll (req, res) {
    const data = await userService.getAll()
    res.json(data)
}

async function create (req, res, next) {
    console.log('req.body', req.body)
    try{
        const data = await userService.create(req.body)
        res.json(data)
    } catch (e) {
        next(e)
    }
}
async function update (req, res, next) {
    console.log('req.body', req.body)
    try{
        const data = await userService.update(req.body)
        res.json(data)
    } catch (e) {
        next(e)
    }
}

module.exports = {
    getAll,
    create,
    update
}
