const router = require('express').Router()
const {
    getAll: getAllCategories,
    createCategory} = require('./controllers/categories')

const {
    getAll: getAllUser,
    create : createUser} = require('./controllers/user')

const {
    getAll: getAllProducts,
    create: createProduct,
    update: updateProduct} = require('./controllers/products')

router.get('/category', getAllCategories)
router.post('/category', createCategory)

router.get('/user', getAllUser)
router.post('/user', createUser)


router.get('/product', getAllProducts)
router.post('/product', createProduct)
router.put('/product', updateProduct)

module.exports = router
