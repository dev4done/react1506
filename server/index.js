const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const port = 3001
const db = require('./db')
const routes = require('./routes')


app.use(cors())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())


app.use(routes)

app.use((req, res, next)=>{
    res.status(404).json({error: 'Not found'})
})

app.use((err, req, res, next)=>{
    console.error(err.code, err)
    if(!err.code){
        res.status(500)
    } else {
        res.status(err.code)
    }
    res.json({error: 'Ops. Smth happen'})
})


db.connection.once('open', function() {
    app.listen(port, () => console.log(`Example app listening on port ${port}!`))
});

